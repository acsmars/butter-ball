In order to run as the Butter-Ball server you must do the following:

1. Install node.js: https://nodejs.org/download/
2. After node.js is installed execute the following via cli:
	1. npm install socketio
	2. npm install express
3. Execute: node index.js

The server should now be listening on port 8080.
